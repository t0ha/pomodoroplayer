-module(smtp).
-compile([export_all]).

send_html(From, To, Subject, Message) ->  % {{{1
    Sendmail = open_port({spawn, "/usr/sbin/sendmail -t"}, []),
    MessageWithHeader = "From: " ++ From ++ "\n" ++
	"To: " ++ To ++ "\n" ++
	"MIME-Version: 1.0\n" ++
	"Content-Type: text/html; charset=utf8\n" ++
	"Subject: " ++ Subject ++ "\n\n" ++
	Message ++ "\n\n", 
    Sendmail ! {self(), {command, MessageWithHeader}},
    Sendmail ! {self(), close}.
