-module(nitrogen_app).
-behaviour(application).
-export([start/2, stop/1]).

%% ===================================================================
%% Application callbacks
%% ===================================================================

start(_StartType, _StartArgs) ->
    application:ensure_all_started(esocial),
    application:ensure_all_started(mnesia),
    application:ensure_all_started(gettexter),
    nitrogen_sup:start_link().

stop(_State) ->
    ok.
