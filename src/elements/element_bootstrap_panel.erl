%% -*- mode: nitrogen -*-
%% vim: ts=4 sw=4 et
-module (element_bootstrap_panel).
-include_lib("nitrogen_core/include/wf.hrl").
-include("records.hrl").
-export([
    reflect/0,
    render_element/1
]).


-spec reflect() -> [atom()].
reflect() -> record_info(fields, bootstrap_panel).

-spec render_element(#bootstrap_panel{}) -> body().
render_element(_Record = #bootstrap_panel{
                          context=Context,
                          header_id = HeaderId,
                          header = Header,
                          body_id = BodyId,
                          body = Body,
                          list_id = ListId,
                          list = List,
                          table_id = TableId,
                          table = Table,
                          footer_id = FooterId,
                          footer = Footer
                           }) ->
    #panel{
       class=[
              "panel",
              "panel-" ++ wf:to_list(Context)
             ],
       body=[
             #panel{
                id = HeaderId,
                class=["panel-heading"],
                show_if = (Header /= undefined),
                body = Header
               },
             #panel{
                id = BodyId,
                class=["panel-body"],
                show_if = (Body /= undefined),
                body = Body
                },
             #list{
                id = ListId,
                class="list-group",
                show_if = (List /= undefined),
                body = List
               },
             #table{
                id = TableId,
                class=["table"],
                show_if = (Table /= undefined),
                rows = Table
               },
             #panel{
                id = FooterId,
                class=["panel-footer"],
                show_if = (Footer /= undefined),
                body = Footer
               }
            ]}.
