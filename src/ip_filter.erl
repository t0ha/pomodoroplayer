%% -*- mode: nitrogen -*-
%% vim: ts=4 sw=4 et
-module (ip_filter).
-compile(export_all).
-include_lib("nitrogen_core/include/wf.hrl").
-include("records.hrl").

main() ->
    wf:session(analytics, false),
    wf:redirect("/").

