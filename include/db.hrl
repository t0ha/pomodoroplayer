%% DB tables records goes here
-record(profile,
        {
         id :: {atom(), non_neg_integer()},
         strict = true :: boolean(),
         work= {25, 0} :: tuple(),
         short = {5, 0} :: tuple(),
         long = {15, 0} :: tuple(),
         roles = [] :: [atom()],
         created :: calendar:datetime(),
         last_login :: calendar:datetime(),
         settings = #{} :: map()
        }).

-record(playlist, {
          id :: non_neg_integer(),
          profile_id :: {atom(), non_neg_integer()},
          platform_id :: {atom(), any()} | undefined,
          name :: binary(),
          songs :: queue:queue(non_neg_integer()),
          settings = #{} :: map()
         }).

-record(task, 
        {
         id :: non_neg_integer(),
         name :: binary(),
         description = <<>>:: binary(),
         profile_id :: {atom(), non_neg_integer()},
         parent = 0 :: non_neg_integer(),
         state = new :: new | working | done,
         estimate = 1 :: pos_integer(),
         done = 0 :: non_neg_integer(),
         failed = 0 :: non_neg_integer(),
         created :: calendar:datetime(),
         deadline :: calendar:date(),
         priority = <<"medium">> :: binary(),
         settings = #{} :: map()
        }).

-record(pomodoro, 
        {
         id :: non_neg_integer(),
         profile_id :: {atom(), non_neg_integer()},
         task_id :: non_neg_integer(),
         is_full = true :: boolean(),
         created :: calendar:datetime(),
         comment :: iodata(),
         data :: map()
        }).

-record(account,
        {
         id :: esocial:esocial(),
         profile_id :: esocial:esocial(),
         settings :: map()
        }).

