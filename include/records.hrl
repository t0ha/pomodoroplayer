%% Include the automatically generated plugins directory
-include("plugins.hrl").

%% Include any application-specific custom elements, actions, or validators below
-record(modal, {?ELEMENT_BASE(element_modal),
        body :: body(),
        buttons :: body()
    }).

-record(bootstrap_button, {?ELEMENT_BASE(element_bootstrap_button),
                           icon :: iodata(),
                           icon_type="glyphicon" :: iodata(),
                           icon_size="normal" :: iodata(),
                           postback :: term(),
                           delegate :: module(),
                           url :: string(),
                           popover :: iodata(),
                           placement = left :: left | right | top | bottom
    }).

-record(bootstrap_panel, {?ELEMENT_BASE(element_bootstrap_panel),
                          context = "default",
                          header_id :: atom(),
                          header,
                          body_id :: atom(),
                          body,
                          list_id :: atom(),
                          list,
                          table_id :: atom(),
                          table,
                          footer_id :: atom(),
                          footer
    }).

-record(bootstrap_popover, {?ACTION_BASE(action_bootstrap_popover),
                            placement = right,
                            title = "",
                            content = "",
                            body = ""
    }).
