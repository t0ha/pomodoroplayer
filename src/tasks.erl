%% -*- mode: nitrogen -*-
%% vim: ts=4 sw=4 et
-module (tasks).
-compile(export_all).
-include_lib("nitrogen_core/include/wf.hrl").
-define(GETTEXT_DOMAIN, 'pomodoro-player').
-include_lib("gettexter/include/shortcuts.hrl").
-include_lib("esocial/include/esocial.hrl").
-include("records.hrl").
-include("db.hrl").

-define(TASK_PRIORITIES, 
        [
         <<"high">>,
         <<"medium">>,
         <<"low">>
        ]).

main() -> common:main().

title() -> "Hello from tasks.erl!".

body(#esocial{platform=Platform, user_id=RUID}) ->  % {{{1
    UID = {Platform, RUID},
    Projects = db:get_tasks(UID, 0),
    projects(Projects).
	
event({new_window, 0}) ->  % {{{1
    wf:wire(#script{script="$(obj('projects')).popover('hide');"}),
    wf:remove("#newProject"),
    wf:insert_bottom("body", new_window("0")),
    wf:wire(#script{script="$('#newProject').modal('show');"});
event({new_window, Level}) ->  % {{{1
    wf:wire(#script{script="$(obj('task_create')).popover('hide');"}),
    wf:remove("#newProject"),
    wf:insert_bottom("body", new_window(wf:to_list(Level))),
   wf:wire(#script{script="$('#newProject').modal('show');"});
    
event(new_project) ->  % {{{1
    Level = wf:to_integer(wf:q(level)),
    Id = wf:to_atom(wf:f("current_project_~p", [Level])),
    PId = wf:to_atom(wf:f("current_project_~p", [Level-1])),
    Parent = case wf:q(PId) of
                 undefined -> 0;
                 P -> wf:to_integer(P)
             end,
    wf:info("New project with level: ~p parent ~p", [Level, Parent]),
    wf:wire(#script{script="$('#newProject').modal('hide')"}),
    case common:q(project_name) of
        "" ->
            ok;
        Name ->
            UID = common:uid(),
            Description = common:q(project_description),
            Due = common:q(deadline),
            Priority = common:q(priority),
            wf:info("Name: ~p", [Name]),
            wf:info("Due: ~p", [Priority]),
            Deadline = parse_due(Due),
            #task{id=TID} = Task = db:new_task(#task{
                                                  name=Name,
                                                  description=Description,
                                                  profile_id=UID,
                                                  priority=wf:to_atom(Priority),
                                                  parent=wf:to_integer(Parent),
                                                  deadline=Deadline}),
            case Level of 
                0 ->
                    wf:insert_bottom(Id, wf:f("<option value='~p'>~ts</option>", [TID, wf:html_encode(Name)])),
                    wf:update(description, description(Description)),
                    wf:set(Id, TID),
                    common:tutorial_next(),
                    wf:wire(#script{script=wf:f("$(obj('~s')).change();", [Id])}),
                    wf:flash(?_(wf:f("Project ~ts created", [Task#task.name]))),
                    wf:update(tasks, []);
                _ ->  
                    common:tutorial_next(),
                    try
                        wf:flash(?_(wf:f("Task ~ts created", [Task#task.name])))
                    catch
                        _:_ -> ok
                    end,
                    wf:insert_bottom(tasks, task(Task))
            end
    end;

event({project_change, Level}) ->  % {{{1
    Id = wf:to_atom(wf:f("current_project_~p", [Level])),
    PID = wf:to_integer(wf:q(Id)),
    project_change(PID);

event({project_remove, 0}) ->  % {{{1
    UID = common:uid(),
    PID = wf:to_integer(wf:q(current_project_0)),
    wf:info("Remove project level ~p to ~p", [0, PID]),
    db:remove(task, PID),
    [#task{id=NPID, name=Name}|_] = Tasks = db:get_tasks(UID),
    wf:wire(#script{script="$(obj('current_project_0')).select2('destroy');"}),
    wf:replace(current_project_0, dropdown(Tasks, 0)),
    wf:info("NPID: ~p", [NPID]),
    wf:set(current_project_0, NPID),
    wf:wire(#script{script="$(obj('current_project_0')).select2({'theme': 'bootstrap'});"}),
    wf:flash(wf:f("Project ~ts removed", [Name])),
    project_change(NPID);
event({project_remove, Level}) ->  % {{{1
    Id = wf:to_atom(wf:f("current_project_~p", [Level])),
    PID = wf:to_integer(wf:q(Id)),
    wf:info("Remove project level ~p to ~p", [Level, PID]),
    #task{parent=Parent} = db:get_by_id(task, PID),
    PTask = db:get_by_id(task, Parent),
    db:remove(task, PID),
    [#task{id=NPID}|_] = db:get_tasks(PTask),
    wf:set(Id, NPID),
    wf:remove(wf:f("option[value=~p]", [PID])),
    project_change(NPID);

event({task_done, PID, Id}) ->  % {{{1
    wf:info("Task ~p done", [PID]),
    Task = db:mark_task_done(PID),
    wf:flash(wf:f("Task ~ts done! Congratulations!", [Task#task.name])),
    wf:remove(Id);

event({task_remove, PID, Id, Name}) ->  % {{{1
    wf:info("Remove task ~p", [PID]),

    db:remove(task, PID),
    wf:flash(wf:f("Task ~ts removed unfinished", [Name])),
    wf:remove(Id);

event({checklist, #task{id=TID}=Task}) ->  % {{{1
    wf:info("Checklisting: ~p", [Task]),
    wf:wire(#script{script="$(obj('add_to_checklist')).popover('hide');"}),
    common:tutorial_next(),
    common:tutorial(4, done_remove, "Completing or deleting the task", "When you finish your task push <i class=\"glyphicon glyphicon-ok-circle\" style=\"color: green;\"></i> to make it \"Done\". If the task is not needed any more, but is not done you can remove it by ckicking on <i class=\"glyphicon glyphicon-remove-circle\" style=\"color: red;\"></i>", right),
    common:tutorial_next(),
    common:tutorial(5, button_playlist, "", "Now you are done with tasks. Let's go on and compose playlists. Click on <i class=\"fa fa-headphones\"></i> to go on."),
    common:tutorial_next(),
    CheckList = wf:session_default(checklist, sets:new()),
    case sets:is_element(TID, CheckList) of
        false ->
            wf:flash(wf:f("Task ~ts added to your checklist", [Task#task.name])),
            wf:session(checklist, sets:add_element(TID, CheckList));
        true ->
            wf:flash(wf:f("Task ~ts removed from your checklist", [Task#task.name])),
            wf:session(checklist, sets:del_element(TID, CheckList))
    end,
    wf:update(checklist, checklist(false));
    


event(Click) ->  % {{{1
    wf:info("Event: ~p", [Click]).

new_window(Level) -> %  {{{1
    Options = ?TASK_PRIORITIES,
    wf:defer(#script{script="$('#newProject select').select2();"}),
    #modal{title=case Level of
                     "0" -> ?_("New Project", wf:session_default(lang, <<"en_US">>));
                     _ -> ?_("New Task", wf:session_default(lang, <<"en_US">>))
                 end,
           body=[
                 #panel{
                    class="form-group",
                    body=
                 #hidden{
                    id=level,
                    class=["form-control"],
                    text=Level
                   }},
                 #panel{
                    class="form-group",
                    body=
                    #textbox{
                       id=project_name,
                       class=["form-control"],
                       placeholder = case Level of 
                                         "0" -> ?_("Project name", wf:session_default(lang, <<"en_US">>));
                                         _ -> ?_("Task name", wf:session_default(lang, <<"en_US">>))
                                     end
                      }},
                 #panel{
                    class="form-group",
                    body=
                 #textarea{
                    id=project_description,
                    class=["form-control"],
                    placeholder = case Level of 
                                      "0" -> ?_("Project description", wf:session_default(lang, <<"en_US">>));
                                      _ -> ?_("Task description", wf:session_default(lang, <<"en_US">>))
                                  end
                   }},
                 #panel{
                    class="form-group",
                    show_if = (Level /= "0"),
                    body=
                 #datepicker_textbox{
                    id=deadline,
                    class=["form-control"],
                    placeholder=?_("Due", wf:session_default(lang, <<"en_US">>))
                   }},
                 #panel{
                    class="form-group",
                    show_if = (Level /= "0"),
                    body=
                 #dropdown{
                    id=priority,
                    class=["form-control"],
                    value = <<"medium">>,
                    options = lists:map(fun(Option) ->
                                                #option{
                                                   value=Option,
                                                   text=Option
                                                  }
                                        end,
                                        Options)
                   }}
                      
                ],
           html_id="newProject",
           buttons=[
                    #link{
                       class="btn",
                       style="color: grey;",
                       body=#span{
                               class=[
                                      "fa", 
                                      "fa-check"
                                     ],
                               text=""
                              },
                       postback=new_project,
                       delegate=?MODULE
                      },
                    #link{
                       class="btn",
                       style="color: grey;",
                       body=#span{
                               class=[
                                      "fa", 
                                      "fa-remove"
                                     ],
                               text=""
                              },
                       data_fields=[
                                   {"dismiss", "modal"}
                                   ]
                      }
                   ]

          }.

project(Projects, Level) -> % {{{1
    common:tutorial(1, projects, "Create a project", "Now it's time to create you first project. Project is a collection of tasks you will be working on. Click on <i class=\"fa fa-plus-square-o\"></i> above and fill in the form you'll see."),
    Icon = case Level of
               0-> #span{
                      class=[
                             "fa", 
                             "fa-briefcase"
                            ],
                      text=""
                     };
               _ -> #span{
                       class=[
                              "fa", 
                              "fa-folder-open"
                             ],
                       text=""
                      }
           end,
    #panel{
       class=["row"],
       body=[
             #panel{
                class=["col-lg-12"],
                body=#panel{
                        class=[
                               "input-group",
                               "select2-bootstrap-prepend",
                               "select2-bootstrap-append"
                              ],
                        body=[
                              #panel{
                                 style="padding: 0 12px;border: #000 0px solid;background: transparent;",
                                 class=[
                                        "input-group-addon"
                                       ],
                                 body=Icon
                                },
                              dropdown(Projects, Level),
                              #panel{
                                 id=projects,
                                 style="padding: 0 12px;border: #000 0px solid;background: transparent;",
                                 class=[
                                        "input-group-addon"
                                       ],
                                 body=[
                                       #bootstrap_button{
                                          icon_type="fa",
                                          icon="plus-square-o",
                                          style="color:grey;display:block;height:15px;",
                                          postback={new_window, Level},
                                          popover=?_("Add project", wf:session_default(lang, <<"en_US">>))
                                         },
                                       #bootstrap_button{
                                          icon_type="fa",
                                          icon="minus-square-o",
                                          style="color:grey;",
                                          actions=#event{
                                                     type=click,
                                                     actions=#confirm{
                                                                text=?_("Do you really want to remove this project?", wf:session_default(lang, <<"en_US">>)),
                                                                postback={project_remove, Level}
                                                               }},
                                          popover=?_("Delete project", wf:session_default(lang, <<"en_US">>))
                                         }
                                      ]
                                }
                             ]}
               }
            ]}.

projects([]) -> % {{{1
    #panel{
       class=["row"],
       body=[
             #panel{
               class=[
                      "col-lg-5",
                       "col-lg-offset-1"
                     ],
               body=#bootstrap_panel{
                                header_id=header,
                                header=project([], 0),
                                body_id=description,
                                body = ?_("Create your first project here", wf:session_default(lang, <<"en_US">>)),
                                table=tasks([])
                      }},
             #panel{
                class=[
                       "col-lg-3",
                       "col-lg-offset-1"
                      ],
               body=#bootstrap_panel{
                       header=?_("CheckList", wf:session_default(lang, <<"en_US">>)),
                       table_id=checklist,
                       table=checklist(false)
                      }
              }
            ]};
projects([#task{description=Description}=Project|_]=Projects) -> % {{{1
    Tasks = db:get_tasks(Project),
    #panel{
       class=["row"],
       body=[
             #panel{
                 class= [
                       "col-xs-8",
                       "col-sm-8",
                       "col-md-6",
                       "col-md-offset-1",
                       "col-lg-6",
                       "col-lg-offset-1"
                        ],
               body=#bootstrap_panel{
                                header_id=header,
                                header=project(Projects, 0),
                                body_id=description,
                                body = description(Description),
                                table=tasks(Tasks)
                      }},
             #panel{
                 class= [
                       "col-xs-4",
                       "col-sm-4",
                       "col-md-3",
                       "col-md-offset-1",
                       "col-lg-3",
                       "col-lg-offset-1"
                        ],
               body=#bootstrap_panel{
                       header=?_("CheckList", wf:session_default(lang, <<"en_US">>)),
                       table_id=checklist,
                       table=checklist(false)
                      }
              }
            ]}.

tasks(Tasks) -> % {{{1
    common:tutorial(2, task_create, "Add tasks to project", "Next step is to add tasks to your project. Click <i style=\"color: green;\" class=\"fa fa-plus\"></i> above and fill in the form."),
    #table{
       id=tasks,
       class="table",
       rows=[
             #tablerow{
                cells=[
                       #tableheader{
                          style="width: 5%; vertical-align: middle;"
                         },
                       #tableheader{
                          style="width: 70%; vertical-align: middle;",
                          text=?_("Task name", wf:session_default(lang, <<"en_US">>))
                         },
                       #tableheader{
                          style="width: 20%; vertical-align: middle;",
                          text=?_("Due", wf:session_default(lang, <<"en_US">>))
                         },
                       #tableheader{
                          id=task_create,
                          style="width: 5%;",
                          body=#bootstrap_button{
                                  icon="plus",
                                  style="color: #5cb85c;",
                                  popover=?_("Add task", wf:session_default(lang, <<"en_US">>)),
                                  postback={new_window, 1}
                                 }
                         }
                      ]} |

             lists:map(fun task/1,
                       Tasks)
            ]}.

task(#task{id=TID, % {{{1
           name=Name,
           deadline=Deadline,
           priority=Priority
          }=Task) ->
    Sub = db:get_tasks(Task), 
    Id = wf:temp_id(),
    common:tutorial(3, add_to_checklist, "Add task to checklist", "Now you have a task. To work on it you should add it to your current checklist. Check this box to do it. You can add tasks form several projects to your checklist. You can see your checklist in the right window."),
    #tablerow{
       id=Id,
       cells=[
              #tablecell{
                 id = add_to_checklist,
                 style="padding: 10px; width: 5%; vertical-align: middle;",
                 body=case length(Sub) of
                          0 ->
                              #checkbox{
                                 checked=is_in_checklist(TID),
                                 postback={checklist, Task},
                                 delegate=?MODULE
                                };
                          N -> 
                              #span{
                                 class="badge",
                                 text=length(Sub)
                                }
                      end},
              #tablecell{
                 style="width: 5%; vertical-align: middle;",
                 text=Name
                },
              #tablecell{
                 style="width: 20%; vertical-align: middle;",
                 body=common:render_date(Deadline)
                },
              %#tablecell{
              %   body=priority(Priority)
              %  },
              #tablecell{
                 id=done_remove,
                 style="width: 5%; vertical-align: middle;",
                 body=[
                       #bootstrap_button{
                         icon="ok-circle",
                         style="color: #5cb85c;",
                         %class="pull-right",
                         postback={task_done, TID, Id},
                         popover=?_("Done", wf:session_default(lang, <<"en_US">>))
                        },
                       #bootstrap_button{
                         icon="remove-circle",
                         %class="pull-right",
                         style="color: #d9534f;",
                         actions=#event{
                                    type=click,
                                    actions=#confirm{
                                               text=?_("Do you really want to remove this task incomplete?", wf:session_default(lang, <<"en_US">>)),
                                               postback={task_remove, TID, Id, Name}
                                              }},
                         popover=?_("Delete task", wf:session_default(lang, <<"en_US">>))
                        }
                      ]
                }
             ]}.

description(Description) -> % {{{1
    Description.

parse_due(undefined) ->  % {{{1
    undefined;
parse_due("") ->  % {{{1
    undefined;
parse_due(Due) when is_list(Due) ->  % {{{1
    [Y, M, D] = string:tokens(Due, "-"),
    {wf:to_integer(Y), wf:to_integer(M), wf:to_integer(D)};
parse_due(Due) ->  % {{{1
    undefined.


project_change(PID) -> % {{{1
    wf:info("Change project to ~p", [PID]),
    #task{description=Description}= Task = db:get_by_id(task, PID),
    SubTasks = db:get_tasks(Task),
    wf:update(description, description(Description)),
    wf:replace(tasks, tasks(SubTasks)).

dropdown(Projects, Level) -> % {{{1
    Id = wf:to_atom(wf:f("current_project_~p", [Level])),
    #dropdown{
       id=Id,
       style="background-color: transparent; height: 46px;",
       class="form-control",
       postback={project_change, Level},
       options=lists:map(
                 fun(#task{
                        id=TID,
                        name=Name
                       }) ->
                         #option{
                            text=Name,
                            value=TID
                           }
                 end,
                 Projects)
      }.

priority(high) -> % {{{1
    #span{
       style="color: #f00000",
       class=[
              "fa", 
              "fa-fire"
             ],
       text=""
      };
priority(medium) -> % {{{1
    #span{
       style="color: #f0f000",
       class=[
              "fa"
             ],
       text=""
      };
priority(low) -> % {{{1
    #span{
       style="color: #00f000",
       class=[
              "fa", 
              "fa-fire"
             ],
       text=""
      };
priority(Any) -> % {{{1
    wf:warning("Wrong priority: ~p", [Any]),
    wf:f("~p", [Any]).

is_in_checklist(TID) -> % {{{1
    CheckList = wf:session_default(checklist, sets:new()),
    sets:is_element(TID, CheckList).

checklist(Current) ->  % {{{1
    CheckListIds = wf:session_default(checklist, sets:new()),
    CurrentTask = wf:session(current_task),
    CheckList = db:get_checklist(CheckListIds),
    lists:map(fun(#task{
                     id=TID,
                     name=Name
                    }) ->
                      Id = wf:temp_id(),
                      Done = db:get_pomodoros_all(TID),
                      #tablerow{
                         id=Id,
                         %class="list-group-item",
                         style="vertical-align: middle;",
                         cells=[
                               #tablecell{
                                  style="width: 5%; vertical-align: middle;",
                                  show_if=Current,
                                  body=#radio{
                                          id=task,
                                          name=task,
                                          checked=(CurrentTask == TID),
                                          value=TID,
                                          style="margin: 5px;",
                                          class=["radio-inline"],
                                          postback={task_selected, TID}}
                                 },
                               #tablecell{
                                  style="vertical-align: middle; overflow: hidden;",
                                  text=Name},
                               #tablecell{
                                  style="width: 5%; vertical-align: middle;",
                                  body=#span{
                                          class="badge",
                                          text=Done}
                                 },
                               #tablecell{
                                  style="width: 5%; vertical-align: middle;",
                                  show_if=Current,
                                  body=[
                                        #bootstrap_button{
                                           icon="ok-circle",
                                           style="color: #5cb85c;",
                                           %class="pull-right",
                                           postback={task_done, TID, Id},
                                           delegate=?MODULE,
                                           popover=?_("Done", wf:session_default(lang, <<"en_US">>))
                                          },
                                        #bootstrap_button{
                                           icon="remove-circle",
                                           %class="pull-right",
                                           style="color: #d9534f;",
                                           postback={task_remove, TID, Id},
                                           delegate=?MODULE,
                                           popover=?_("Delete task", wf:session_default(lang, <<"en_US">>))
                                          }
                                       ]
                                 }
                              ]}
              end,
              CheckList).

