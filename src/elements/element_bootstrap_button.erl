%% -*- mode: nitrogen -*-
%% vim: ts=4 sw=4 et
-module (element_bootstrap_button).
-include_lib("nitrogen_core/include/wf.hrl").
-include("records.hrl").
-export([
    reflect/0,
    render_element/1
]).

%% Move the following record definition to records.hrl:
-spec reflect() -> [atom()].
reflect() -> record_info(fields, bootstrap_button).

-spec render_element(#bootstrap_button{}) -> body().
render_element(#bootstrap_button{
                  icon_type=Type,
                  icon_size=Size,
                  icon=Icon,
                  postback=Postback,
                  delegate=Delegate,
                  url=Url,
                  data_fields=DataFields,
                  class=Class,
                  style=Style,
                  placement=Placement,
                  popover=Popover
                 }) ->
    #link{
       class=Class,
       style=Style,
       body=#span{
               class=[
                      wf:to_list(Type),
                      wf:f("~s-~s", [Type, Icon]),
                      wf:f("~s-~s", [Type, Size])
                     ],
               text=""
              },
       title=Popover,
       data_fields=DataFields ++ [
                        {toggle, "tooltip"},
                        {placement, Placement}
                       ],
       postback=Postback,
       delegate=wf:coalesce([Delegate, wf:page_module()]),
       url=Url
      }.
