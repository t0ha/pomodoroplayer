%% -*- mode: nitrogen -*-
%% vim: ts=4 sw=4 et
-module (common).
-compile(export_all).
-include_lib("nitrogen_core/include/wf.hrl").
-include("records.hrl").
-include("db.hrl").
-define(GETTEXT_DOMAIN, 'pomodoro-player').
-include_lib("gettexter/include/shortcuts.hrl").
-include_lib("esocial/include/esocial.hrl").

main() -> % {{{1
    gettexter:textdomain(?GETTEXT_DOMAIN), % domain for current process
    gettexter:bindtextdomain(?GETTEXT_DOMAIN, "./priv/locales"),
    lang(),
    wf:session(captcha, undefined),
    case wf:user() of
        undefined ->
            case wf:q(code) of
                undefined ->
                    #template{file="./templates/index.html"};
                _ -> 
                    #template{file="./templates/bare.html", 
                              module_aliases=[{'page', index}],
                              bindings=[{'User', undefined}]}
            end;

        User ->
            wf:session(state, wait),
            _ = maybe_new(),
            wf:wire(#api{name=timer_event,
                         tag=audio,
                         delegate=common}),

            #template{file="./templates/bare.html", 
                      bindings=[{'User', User}]}
    end.
% }}}
title() -> ?_("Pomodoro Player - Productivity inspired by music", wf:session_default(lang, <<"en_US">>)). 

lang() ->  % {{{1
    case wf:q(lang) of
        undefined ->
            lang(wf:session(lang));
        Locale ->
            wf:session(lang, wf:to_binary(Locale)),
            gettexter:setlocale(lc_messages, wf:to_binary(Locale)),  % locale for cur
            lang(Locale)
    end.

lang(Str) when is_list(Str) -> lang(wf:to_binary(Str)); % {{{1
lang(<<"ru_RU">>) -> "ru"; % {{{1
lang(<<"en_US">>) -> "en"; % {{{1
lang(undefined) -> % {{{1
    gettexter:setlocale(lc_messages, <<"en_US">>),  % locale for cur
    "en".

analytics() -> % {{{1
    Session = wf:session_default(analytics, true),
    wf:info("Session filter: ~p", [not Session]),
    case wf:peer_ip([{127,0,0,1}]) of
        %{127, 0, 0, 1} -> "";
        _ when Session ->
            #template{file="./templates/ga.html"};
        _ -> ""
    end.

timer() -> % {{{1
    [
     #panel{
        class=[
               "navbar-form",
               "btn-group"
              ],
        style="margin-top: 15px",
        body=#panel{
                class=["form-group"],
                body=#panel{
                        class=["input-group"],
                        body=[
                              #textbox{
                                 id=timer,
                                 disabled=true,
                                 class="form-control",
                                 style="font-size: 150%; color: red; text-align: center;",
                                 text="25:00"
                                },
                              timer_btn(start)

                             ]}
               }
       }
    ].

nav(User) -> % {{{1
    #list{
       numbered=false,
       show_if= User /= undefined,
       class=[
              "nav",
              "navbar-nav",
              "navbar-right"
             ],
       body=[
             #listitem{
                id=button_dashboard,
                body=#bootstrap_button{
                        icon_type="fa",
                        icon_size="lg",
                        icon="dashboard",
                        url="/",
                        popover=?_("Dashboard", wf:session_default(lang, <<"en_US">>))
                       }
            },
             #listitem{
                id=button_playlist,
                body=#bootstrap_button{
                        icon_type="fa",
                        icon_size="lg",
                        icon="headphones",
                        popover=?_("Playlists editor", wf:session_default(lang, <<"en_US">>)),
                        url="/playlist"
                       }
               },
             #listitem{
                id=button_tasks,
                body=#bootstrap_button{
                        icon_type="fa",
                        icon_size="lg",
                        icon="calendar",
                        popover=?_("Tasks", wf:session_default(lang, <<"en_US">>)),
                        url="/tasks"
                       }
               },
             #listitem{
                id=button_admin,
                body=#bootstrap_button{
                        show_if=wf:role(admin),
                        icon_type="fa",
                        icon_size="lg",
                        icon="users", url="/admin" }
               },
            #template{file="./templates/lang.html"}
            ]}.

flash() -> % {{{1
    #flash{}.

event(send_captcha) ->  % {{{1
    Access = wf:user(),
    wf:wire(#script{script="$('#captcha').modal('hide')"}),
    Captcha = wf:q(captcha),
    Sid = wf:q(sid),
    wf:remove("#captcha"),
    wf:session(captcha, undefined),
    wf:info("Captcha: ~p; Sid: ~p", [Captcha, Sid]),
    esocial:captcha(Access, {Sid, Captcha}),
    wf:redirect("/playlist");
event(play) ->  % {{{1
    case wf:session(current_task) of
        undefined ->
            wf:wire(#alert{text="Please, select task to work on"});
        _ ->
            wf:state(playing, undefined),
            wf:wire(#script{script="$(obj('timer_btn')).popover('hide');"}),
            wf:role(new, false),
            wf:state(playing, undefined),
            wf:session(state, run),
            timer_function({0, 0, 0}, run),
            wf:wire(#event{postback=next, delegate=?MODULE})
    end;

event(pause) ->  % {{{1
    wf:state(playing, stop),
    enable_controls(),
    wf:replace(timer_btn, timer_btn(start)),
    wf:eager(".songs", #remove_class{class="active"}),
    common:pause(),
    Start = wf:session_default(play_start, 1500),
    skip_songs(calendar:seconds_to_time(Start - 10)),
    wf:session(state, wait);

event(loaded) ->  % {{{1
    wf:info("Loaded..."),
    Play = wf:state(playing),
    case wf:session(state) of
        wait when Play == undefined ->
            wf:session(state, loaded);
        _ -> ok
    end;
event(next) ->  % {{{1
    Play = wf:state(playing),
    wf:info("Next old ~p", [Play]),
    common:next(Play);

event(Click) ->  % {{{1
    wf:info("Event in ~p: ~p", [?MODULE, Click]).

api_event(timer_event, _, [Duration, Duration, Duration]) -> % {{{1
    wf:info("Duration = : ~p", [Duration]),
    Play = wf:state(playing),
    wf:info("Next ~p", [Play]),
    common:next(Play);
api_event(timer_event, _, [Duration, Duration, AudioTime]) -> % {{{1
    %wf:info("AudioTime: ~p", [AudioTime]),
    State = wf:session_default(state, run),
    Start = wf:session_default(play_start, 1500),
    %wf:info("Timer (~p, ~p, ~p)", [Start, Duration, State]),
    Now = try calendar:seconds_to_time(wf:to_integer(Start - AudioTime))
          catch error:_ -> {0, 0, 0} end,
    timer_function(Now, State);
api_event(timer_event, Tag, [ServerDuration, Duration, AudioTime]) -> % {{{1
    wf:info("ServerDuration: ~p", [ServerDuration]),
    wf:info("Duration: ~p", [Duration]),
    wf:info("AudioTime: ~p", [AudioTime]),
    Start = wf:session_default(play_start, 1500),
    NewStart = Start - (ServerDuration - Duration),
    wf:session(play_start, NewStart),
    api_event(timer_event, Tag, [Duration, Duration, AudioTime]);
    
api_event(ApiEvent, Tag, Args) -> % {{{1
    wf:warning("Wrong API event: ~p ~p ~p", [ApiEvent, Tag, Args]).


play(Id) ->  % {{{1
    FID = id_from_db("audio", Id),
    wf:info("Fid: ~p", [FID]),
    wf:wire(#script{script="obj('" ++ wf:to_list(FID) ++ "').play()"}).

pause() ->  % {{{1
    wf:wire(#script{script="$('audio').each(function(_,o){o.currentTime=0.0;o.pause();});"}).

timer_function({0, 0, 0}, run) -> % {{{1
    TimeQueue = wf:session_default(timer_periods, queue:new()),
    wf:info("TimeQueue: ~p", [queue:to_list(TimeQueue)]),
    {_, M, S} = Time = case queue:out(TimeQueue) of
                           {empty, _} ->
                               {0, 25, 0};
                           {{value, Secs}, TimeQueue1} ->
                               wf:session(timer_periods, queue:in(Secs, TimeQueue1)),
                               calendar:seconds_to_time(Secs)
                       end,
    if M >= 25; M >=24, S >= 60 ->
           disable_controls(),
           wf:replace(timer_btn, timer_btn(stop)),
           timer_function(Time, run);
       M < 20 ->
           enable_controls(),
           update_task(true),
           timer_function(Time, run)
    end;

timer_function({H, 0, 0}, run) when H > 0 ->  % {{{1
    timer_function({H-1, 60, 60}, run);
timer_function({H, M, 0}, run) when M > 0 ->  % {{{1
    timer_function({H, M-1, 60}, run);

timer_function({0, M, S}, run) ->  % {{{1
    wf:set(timer, wf:f("~2..0w:~2..0w", [M, S]));
timer_function({H, _M, _S}=T, run) when H > 0 ->  % {{{1
    wf:set(timer, format_time(T));
timer_function(T, S) ->  % {{{1
    wf:warning("Wrong timer_function call (~p, ~p)", [T, S]).



next(undefined) ->  % {{{1
    PlayList = wf:session(playlist_queue),
    case queue:out(PlayList) of
        {{value, #esocial_track{
            id = AID,
            duration = Duration
           }=I}, PlayList1} ->
            wf:eager(".songs", #remove_class{class="active"}),
            wf:wire(playlist:get_playlist_id(AID),
                    #add_class{class="active"}),
            TimeS = wf:q(timer),
            wf:info("TimeS: ~p", [TimeS]),
            case calendar:time_to_seconds(parse_time(TimeS)) of 
                Start when Start > 10 ->
                    wf:session(play_start, Start),

                    wf:eager(#script{script="currentTime=0.0"}),
                    wf:defer(id_from_db("audio", AID),
                             #event{type=timeupdate,
                                    actions=#script{script="timer_event(this, " ++ wf:to_list(Duration) ++ ");"},
                                    delegate=?MODULE}),
                    wf:wire(id_from_db("audio", AID),
                            #event{type=error,
                                   postback=next,
                                   delegate=?MODULE}),
                    common:play(AID),
                    wf:session(playlist_queue, queue:in(I, PlayList1));
                R ->
                    wf:info("R: ~p", [R]),
                    timer_function({0, 0, 0}, run),
                    wf:wire(#event{postback=next, delegate=?MODULE})
            end;
        O -> 
            wf:warning("Other: ~p", [O]),
            ok
    end;
next(_) ->  % {{{1
    ok.

play_lists(UID, Id) ->  % {{{1
    wf:info("UI?D: ~p", [UID]),
    Playlists = db:get_playlists(UID),
    #dropdown{
       id=Id,
       postback={playlist_change, UID, Id},
       class=["form-control"],
       value=wf:session(Id),
       delegate=wf:page_module(),
       options=lists:map(
                 fun(#playlist{
                        id=PID,
                        name=Name}) ->
                         #option{
                            text=Name,
                            value=PID
                           }
                 end, 
                 Playlists)
      }.

id_from_db(Prefix, Id) when is_binary(Id); is_list(Id) -> % {{{1
    wf:to_atom(wf:f("~ts_~ts", [Prefix, Id]));
id_from_db(Prefix, Id) -> % {{{1
    wf:to_atom(wf:f("~ts_~p", [Prefix, Id]));
id_from_db(Prefix, {S, Id}) when is_binary(Id); is_list(Id) -> % {{{1
    wf:to_atom(wf:f("~ts_~ts_~ts", [Prefix, S, Id]));
id_from_db(Prefix, {S, Id}) -> % {{{1
    wf:to_atom(wf:f("~ts_~ts_~p", [Prefix, S, Id])).

enable_controls() -> % {{{1
    wf:enable(work),
    wf:enable(rest),
    wf:enable(task),
    wf:wire(#script{script="$('.wfid_task').attr('disabled', false);"}).

disable_controls() -> % {{{1
    wf:disable(work),
    wf:disable(rest),
    wf:wire(#script{script="$('.wfid_task').attr('disabled', true);"}).

update_task(Full) -> % {{{1
    TID = wf:session(current_task),
    db:add_pomodoro(TID, Full),
    wf:update(checklist,
              tasks:checklist(true)).

skip_songs({_, M, S}) when M >= 25; M >= 24, S >= 60 -> % {{{1
    wf:info("Nothing to skip"),
    TimeQueue = wf:session(timer_periods),
    case queue:out(TimeQueue) of
        {{value, Secs}, TimeQueue1} when Secs >= 1500 ->
            wf:info("Nothing to fix in time queue"),
            wf:session(timer_periods, queue:in_r(Secs, TimeQueue1)),
            ok;
        {{value, Secs}, TimeQueue1} ->
            wf:info("Fixing time queue. Moving pomodoro to start"),
            wf:session(timer_periods, move_back(TimeQueue)),
            wf:session(playlist_queue, move_back(wf:session(playlist_queue)));
        {empty, _} -> 
            wf:info("Time queue is empty"),
            ok
    end;
skip_songs({_, 0, 0}) -> % {{{1
    TimeQueue = wf:session(timer_periods),
    case queue:out(TimeQueue) of
        {{value, Secs}, _TimeQueue1} when Secs >= 1500 ->
            wf:info("Nothing to skip. Next period ~p", [Secs]),
            ok;
        {{value, Secs}, TimeQueue1} ->
            wf:info("Skipping rest time: ~p", [Secs]),
            wf:session(timer_periods, queue:in(Secs, TimeQueue1)),
            skip_songs(calendar:seconds_to_time(Secs));
        {empty, _} -> 
            wf:info("Time queue is empty"),
            ok
    end;
skip_songs({_, M, S}=T) -> % {{{1
    Playlist = wf:session(playlist_queue),
    Secs = calendar:time_to_seconds(T),
    wf:info("Skipping rest of ~p Secs: ~p time", [T, Secs]),
    case queue:out(Playlist) of
        {{value, #esocial_track{duration = D} = Song}, Playlist1} when D > Secs ->
            wf:info("Time already skipped ~p time", [Secs]),
            skip_songs({0, 0, 0});
        {{value, #esocial_track{duration = D} = Song}, Playlist1} ->
            T1 = calendar:seconds_to_time(Secs - D),
            Playlist2 = queue:in(Song, Playlist1),
            wf:session(playlist_queue, Playlist2),
            skip_songs(T1);
        {empty, _} ->
            ok
    end.

move_back(Queue) -> % {{{1
    case queue:out_r(Queue) of
        {{value, V}, Queue1} ->
            queue:in_r(V, Queue1);
        {empty, Queue} ->
            Queue
    end.
        
timer_btn(start) ->  %{{{1
    #span{
       id=timer_btn,
       class="input-group-btn",
       body=
       #button{
          class=[
                 "btn",
                 "btn-success"
                ],
          postback=play,
          delegate=?MODULE,
          body=#span{
                  class=["fa", "fa-play"], 
                  text=""
                 }
         }};
timer_btn(stop) -> %{{{1
    #span{
       id=timer_btn,
       class="input-group-btn",
       body=
       #button{
          class=["btn", "btn-danger"],
          postback=pause,
          delegate=?MODULE,
          body=#span{
                  class=["fa", "fa-stop"], 
                  text=""
                 }
         }}.

maybe_captcha({captcha, {Sid, Img}}) ->
    case wf:session(captcha) of
        undefined ->
            wf:session(captcha, true),
            wf:insert_bottom("body", #modal{
                                        html_id="captcha",
                                        title = ?_(<<"Captcha input needed">>, wf:session_default(lang, <<"en_US">>)),
                                        body=[
                                              #image{image=Img},
                                              #textbox{
                                                 id=captcha,
                                                 class=["form-controll"]
                                                },
                                              #hidden{id=sid, text=Sid}
                                             ],
                                        buttons=#bootstrap_button{
                                                   icon_type="fa",
                                                   icon="check",
                                                   delegate=?MODULE,
                                                   postback=send_captcha,
                                                   popover="OK"
                                                  }
                                       }),
            wf:wire(#script{script="$('#captcha').modal('show')"}),
            [];
        true -> []
    end;
maybe_captcha(Any) -> % {{{1
    Any.

render_date({Y, M, D}) -> % {{{1
    wf:f("~4..0w-~2..0w-~2..0w", [Y, M, D]);
render_date(undefined) -> % {{{1
    ?_("no due date", wf:session_default(lang, <<"en_US">>));
render_date(Any) -> % {{{1
    wf:f("~p", [Any]).

maybe_new() -> % {{{1
    case wf:q(new_user) of
        undefined -> ok;
        "" -> ok;
        _ -> 
            wf:role(new, true)
    end.

tutorial(Step, Target, Title, Text) -> % {{{1
    tutorial(Step, Target, Title, Text, bottom).

tutorial(Step, Target, Title, Text, Direction) -> % {{{1
    wf:info("Tutor: ~p ~p", [wf:state(step), Step]),
    case {wf:role(new), wf:state_default(step, {1, 1})} of
        {true, {S1, S2}} when S2 >= S1, S1 == Step ->
            wf:info("Tutor: ~p ~p", [wf:state(step), Step]),
            wf:state(step, {S1 + 1, S1}),
            wf:defer(#bootstrap_popover{
                       target=Target,
                       placement = Direction,
                       title = wf:js_escape(?_(Title, wf:session_default(lang, <<"en_US">>))),
                       body = wf:js_escape(?_(Text, wf:session_default(lang, <<"en_US">>)))
                      });
        _ -> ok
    end.

tutorial_next() -> % {{{1
    {S1, S2} =  wf:state_default(step, {1, 1}),
    wf:state(step, {S1, S2 + 1}).

parse_time(String) when is_list(String) ->  % {{{1
    case string:tokens(String, ":") of
        [M, S] ->
            {0, wf:to_integer(M), wf:to_integer(S)};
        [H, M, S] ->
            {wf:to_integer(H), wf:to_integer(M), wf:to_integer(S)}
    end.

format_time(T) when is_integer(T) -> % {{{1
    format_time(calendar:seconds_to_time(T));
format_time({0, M, S}) -> % {{{1
    wf:f("~2..0w:~2..0w", [M, S]);
format_time({H, M, S}) -> % {{{1
    wf:f("~2..0w:~2..0w:~2..0w", [H, M, S]).

uid() -> % {{{1
    #esocial{user_id=RUID, platform=Platform} = wf:user(),
    {Platform, RUID}.

uid(UID) -> % {{{1
    #esocial{platform=Platform} = wf:user(),
    {Platform, UID}.


q(Id) -> % {{{1
    q(Id, "").

q(Id, Default) -> % {{{1
    case wf:q(Id) of
        undefined -> Default;
        "" -> Default;
        V -> unicode:characters_to_binary(string:strip(V))
    end.
