%% -*- mode: nitrogen -*-
%% vim: ts=4 sw=4 et
-module (playlist).
-compile(export_all).
-include_lib("nitrogen_core/include/wf.hrl").
-define(GETTEXT_DOMAIN, 'pomodoro-player').
-include_lib("gettexter/include/shortcuts.hrl").
-include_lib("esocial/include/esocial.hrl").
-include("records.hrl").
-include("db.hrl").

main() -> common:main().

%title() -> ?_("Hello from playlist.erl!", wf:session_default(lang, <<"en_US">>)).

body(#esocial{platform=Platform, user_id=RUID}=Access) ->  % {{{1
    UID = {Platform, RUID},
    Audio = get_audio(),
    Playlists = db:get_playlists(UID),
    PlatformPlaylists = common:maybe_captcha(esocial:playlists(Access)),
    lists:foreach(fun(#esocial_playlist{
                         id = AID,
                         name = Name,
                         tracks = Tracks
                        }) ->
                          db:sync_playlist(#playlist{
                                              platform_id={Platform, AID},
                                              name=Name,
                                              profile_id=UID,
                                              songs=queue:from_list(Tracks)})
                  end,
                  PlatformPlaylists),
    wf:info("esocial playlists: ~p", [PlatformPlaylists]),
    PlayList = case Playlists of
                   [] ->#playlist{};
                   [P|_] -> P
               end,
    common:tutorial(1, create_playlist, "Create playlist", "Now it's time to create you first playlist. Click on <i class=\"fa fa-plus-square-o\"></i> above and fill in play list name in the form you'll see."),
    [
     #panel{
        class=["row"],
        body=[
              #panel{
                 class= [
                       "col-xs-7",
                       %"col-xs-offset-1",
                       "col-sm-6",
                       "col-sm-offset-1",
                       "col-md-5",
                       "col-md-offset-1",
                       "col-lg-5",
                       "col-lg-offset-1"
                        ],
                 body=#bootstrap_panel{
                         header = ?_("All Music", wf:session_default(lang, <<"en_US">>)),
                         body=#panel{
                                 class=["row"],
                                 body=[
                                       #panel{
                                          class=[
                                                 "col-xs-12",
                                                 "col-sm-12",
                                                 "col-md-12",
                                                 "col-lg-12"
                                                ],
                                          body=render_search()
                                         }
                                      ]},
                         table_id=serch_table,
                         table=lists:map(fun(S) -> list_row(S, true) end,
                                         Audio)
                        }
                },
              #panel{
                class=[
                       "col-xs-5",
                       %"col-xs-offset-1",
                       "col-sm-4",
                       "col-sm-offset-1",
                       "col-md-4",
                       "col-md-offset-1",
                       "col-lg-4",
                       "col-lg-offset-1"
                      ],
                 body=#bootstrap_panel{
                         header = ?_(<<"Current playlist">>, wf:session_default(lang, <<"en_US">>)),
                         body=#panel{
                                 class=["row"],
                                 body=[
                                       #panel{
                                          class=[
                                                 "col-xs-8",
                                                 "col-sm-9",
                                                 "col-md-9",
                                                 "col-lg-9"
                                                ],
                                          body=common:play_lists(UID, current_playlist)
                                         },
                                       #panel{  
                                          id = create_playlist,
                                          class=[
                                                 "col-xs-2",
                                                 "col-sm-1",
                                                 "col-md-1",
                                                 "col-lg-1",
                                                 "pull-right"
                                                ],
                                          style="padding: 0 5px;",
                                          body=[
                                                #bootstrap_button{
                                                   style="color: grey;height:15px;display:block;",
                                                   icon_type="fa", 
                                                   icon="plus-square-o",
                                                   popover=?_("Create playlist", wf:session_default(lang, <<"en_US">>)),
                                                   postback = new_window
                                                  },
                                                #bootstrap_button{
                                                   icon_type="fa",
                                                   icon="minus-square-o",
                                                   style="color: grey;",
                                                   popover=?_("Delete playlist", wf:session_default(lang, <<"en_US">>)),
                                                   actions=#event{
                                                              type=click,
                                                              actions=#confirm{
                                                                         text=?_("Do you really want to remove this playlist?", wf:session_default(lang, <<"en_US">>)),
                                                                         postback=playlist_remove
                                                                        }}
                                                  }
                                               ]}
                                      ]},
                         table_id=show_playlist,
                         table=render_playlist(PlayList)
                        }
                }
             ]}
    ].
	
event({play, Id, AID}) ->  % {{{1
    wf:info("Play"),
    {PlayingId, PlayingAID} = wf:state_default(playing, {Id, AID}),
    wf:replace(PlayingId, play_button(PlayingId, PlayingAID, play)),
    wf:state(playing, {Id, AID}),
    wf:replace(Id, play_button(Id, AID, pause)),
    common:pause(),
    common:play(AID);
event(new_window) ->  % {{{1
    wf:wire(#script{script="$(obj('create_playlist')).popover('hide');"}),
    common:tutorial_next(),
    wf:remove("#newPlaylist"),
    wf:insert_bottom("body",
                     #modal{
                        title=?_("Create playlist", wf:session_default(lang, <<"en_US">>)),
                        body=#textbox{
                                id=playlist_name,
                                class=["form-control"],
                                placeholder=?_("Playlist name", wf:session_default(lang, <<"en_US">>))
                               },
                        html_id="newPlaylist",
                        buttons=[
                                 #bootstrap_button{
                                    id=create,
                                    style="color: grey;",
                                    icon_type="fa",
                                    icon="check",
                                    postback=new_playlist,
                                    popover="Create playlist"
                                   },
                                 % TODO
                                 %#bootstrap_button{
                                 %   icon="remove",
                                 %   postback=new_playlist,
                                 %   popover="Delete playlist"
                 %  }
                 #link{
                    class="btn",
                    style="color: grey;",
                    body=#span{
                            class=[
                                   "fa", 
                                   "fa-remove"
                                  ],
                            text=""
                           },
                    data_fields=[
                                 {"dismiss", "modal"}
                                ]
                   }
                ]

       }),
    wf:wire(#script{script="$('#newPlaylist').modal('show');"});

event({pause, Id, AID}) ->  % {{{1
    wf:info("Pause"),
    %wf:state(playing, undefined),
    wf:replace(Id, play_button(Id, AID, play)),
    common:pause();

event({add, Id, Track}) ->  % {{{1
    wf:info("Add ~p to playlist", [Id]),
    wf:wire(#script{script="$(\".songs:first-child>td:nth-child(3)\").popover('hide');"}),
    common:tutorial_next(),
    PID = wf:q(current_playlist),
    #playlist{songs=Songs} = PlayList = db:get_playlist(wf:to_integer(PID)),
    NewPlayList = PlayList#playlist{songs=queue:in(Id, Songs)},
    db:save(NewPlayList),
    wf:update(show_playlist, render_playlist(NewPlayList)),
    wf:flash(wf:f("New track ~ts added to playlist ~ts", [Track, PlayList#playlist.name])),
    common:pause();

event({remove, Id, N, Track}) ->  % {{{1
    wf:info("Remove ~p from playlist", [Id]),
    wf:wire(#script{script="$(\".wfid_show_playlist tr:first-child>td:nth-child(2)\").popover('hide');"}),
    common:tutorial_next(),
    PID = wf:q(current_playlist),
    #playlist{songs=Songs} = PlayList = db:get_playlist(wf:to_integer(PID)),
    {SongsL, SongsR} = queue:split(N-1, Songs),
    NewSongs = queue:join(SongsL, queue:tail(SongsR)),
    NewPlayList = PlayList#playlist{songs=NewSongs},
    db:save(NewPlayList),
    wf:flash(wf:f("Track ~ts removed from playlist ~ts", [Track, PlayList#playlist.name])),
    wf:update(show_playlist, render_playlist(NewPlayList));

event(new_playlist) ->  % {{{1
    wf:wire(#script{script="$('#newPlaylist').modal('hide')"}),
    case wf:q(playlist_name) of
        undefined ->
            ok;
        "" ->
            ok;
        Name ->
            #playlist{id=Id} = PlayList = db:new_playlist(Name, common:uid()),
            wf:set(playlist_name, ""),
            wf:insert_bottom(current_playlist, wf:f("<option value='~p'>~ts</option>", [Id, wf:html_encode(Name)])),
            wf:set(current_playlist, Id),
            wf:wire(#script{script="$(obj('current_playlist')).change();"}),
            wf:flash(wf:f("PlayList ~ts created", [PlayList#playlist.name])),
            wf:update(show_playlist, render_playlist(PlayList))
    end;

event({playlist_change, _UID, current_playlist}) ->  % {{{1
    PID = wf:q(current_playlist),
    wf:info("Change playlist to ~p", [PID]),
    PlayList = db:get_playlist(wf:to_integer(PID)),
    wf:update(show_playlist, render_playlist(PlayList));

event(search) ->  % {{{1
    Term = wf:q(search),
    Audio = search_audio(Term),
    wf:update(serch_table,
              lists:map(fun(S) ->
                                list_row(S, true) 
                        end,
                        Audio));
event(playlist_remove) ->  % {{{1
    UID = common:uid(),
    PID = wf:q(current_playlist),
    wf:info("Remove playlist ~p", [PID]),
    db:remove(playlist, wf:to_integer(PID)),
    [#playlist{id=NID} = PlayList|_] = db:get_playlists(UID),
    wf:wire(#script{script="$(obj('current_playlist')).select2('destroy');"}),
    wf:replace(current_playlist, common:play_lists(UID, current_playlist)),
    wf:set(current_playlist, NID),
    Work = wf:session_default(work, NID),
    Rest = wf:session_default(rest, NID),
    _ = case {Work, Rest} of
        {PID, PID} ->
            wf:session(work, NID),
            wf:session(rest, NID);
        {PID, _} ->
            wf:session(work, NID);
        {_, PID} ->
            wf:session(rest, NID);
        _ -> ok
    end,
    wf:wire(#script{script="$(obj('current_playlist')).select2({'theme': 'bootstrap'});"}),
    wf:flash(wf:f("PlayList ~ts removed", [PlayList#playlist.name])),
    wf:update(show_playlist, render_playlist(PlayList));


event(Click) ->  % {{{1
    wf:info("Event: ~p", [Click]).

sort_event(PID, Items) ->  % {{{1
    PlayList = db:get_playlist(wf:to_integer(PID)),
    Songs = queue:from_list(Items),
    wf:info("Playlist ~p: ~p", [PID, Items]),
    db:save(PlayList#playlist{songs=Songs});

sort_event(Tag, Items) ->  % {{{1
    wf:info("Sorted ~p items ~p", [Tag, Items]).

list_row(#esocial_track{  % {{{1
            artist = A,
            id = AID,
            uri = URL,
            duration = D,
            name = T}, Editable) ->
    AudioID = wf:to_list(common:id_from_db("audio", AID)),
    Duration = common:format_time(D),
    Audio = [
             "<audio class='wfid_",
             AudioID,
             "' src='",
             URL,
             "' ",
             "preload='auto' ",
             %"controls='on'", 
             ">",
             "</audio>"
            ],
    wf:defer(#script{script="ended('" ++ AudioID ++ "');"}),

    #tablerow{
       id=get_playlist_id(AID),
       class="songs",
       cells=[
              #tablecell{
                 body=Audio
                },
              #tablecell{
                 text=A
                },
              #tablecell{
                 text=T
                },
              #tablecell{
                 style="vertical-align: middle;",
                 body=#span{
                         class="badge",
                         text=Duration
                }},
              #tablecell{
                 show_if=Editable,
                 %style="padding: 8px 0;",
                 body=[
                       play_button(common:id_from_db("play", AID), AID, play),
                       #bootstrap_button{
                          icon="plus",
                          style="color: grey;",
                          postback={add, AID, <<A/bytes, " - ", T/bytes>>},
                          popover=?_("Add to playlist", wf:session_default(lang, <<"en_US">>))

                         }
                      ]}
             ]}.

play_button(Id, AID, State) ->  % {{{1
    #bootstrap_button{
       id=Id,
       style="color: grey;",
       postback={State, Id, AID},
       icon=wf:f("~p", [State]),
       popover=?_("Listen / Stop", wf:session_default(lang, <<"en_US">>))
      }.
render_search() -> % {{{1
    #panel{
       class=["input-group", "input-group-sm"],
       body=[
             #span{
                class="input-group-addon",
                body="<i class='fa fa-search'></i>"
               },
             #textbox{
                id=search,
                class="form-control",
                placeholder="Search music",
                postback=search
               }
            ]}.

render_playlist(#playlist{songs=undefined}) ->  % {{{1
    common:tutorial(2, ".songs:first-child>td:nth-child(3)", "Add tracks to playlist", "Now you have a playlist. It's time to add tracks. Click on <i class=\"fa fa-plus\"></i> to do it. You can listen the track before adding it by clicking <i class=\"fa fa-play\"></i>.<br> To continue you need to create two playlists with tracks: for work and breaks.", right),
    [];
render_playlist(#playlist{id=PID, songs=Songs}) ->  % {{{1
    common:tutorial(2, ".songs:first-child>td:nth-child(3)", "Add tracks to playlist", "Now you have a playlist. It's time to add tracks. Click on <i class=\"fa fa-plus\"></i> to do it. You can listen the track before adding it by clicking <i class=\"fa fa-play\"></i>.<br> To continue you need to create two playlists with tracks: for work and breaks.", right),
    common:tutorial(3, ".wfid_show_playlist tr:first-child>td:nth-child(2)", "Remove tracks from playlist", "To remove track from playlist click <i class=\"fa fa-minus\"></i> near it. Try it now!", bottom),
    IsEmpty = queue:is_empty(Songs),
    if IsEmpty -> [];
       true -> 
           maybe_dashboard(),
           Audio = get_audio(Songs),
           %wf:info("Playlist: ~p", [Audio]),
           lists:map(
             fun({N,
                  #esocial_track{
                     artist = A,
                     id = AID,
                     uri = URL,
                     duration=Duration,
                     name = T}}) ->
                     #tablerow{
                        cells=[
                               #tablecell{
                                  text = <<A/bytes, " - ", T/bytes>>
                                 },
                               #tablecell{
                                  style="vertical-align: middle;",
                                  body=#span{
                                          class="badge",
                                          text=common:format_time(Duration)
                                         }},
                               #tablecell{
                                  body=#bootstrap_button{
                                          icon="minus",
                                          style="color: grey;",
                                                   actions=#event{
                                                              type=click,
                                                              actions=#confirm{
                                                                         text=?_("Do you really want to remove this song from playlist?", wf:session_default(lang, <<"en_US">>)),
                                                                         postback={remove, AID, N, <<A/bytes, " - ", T/bytes>>}
                                                                        }},
                                          popover=?_("Remove from playlist", wf:session_default(lang, <<"en_US">>))
                                         }
                                 }
                              ]}
             end, 
             Audio)
    end.

get_audio() ->  % {{{1
    Access = wf:user(),
    common:maybe_captcha(esocial:user_tracks(Access)).

get_audio(Songs) ->  % {{{1
    Access = wf:user(),
    SIDs =  queue:to_list(Songs),
    AudioRaw = common:maybe_captcha(esocial:tracks(Access, SIDs)),
    Audios = order_playlist(AudioRaw, Songs),
    lists:zip(lists:seq(1, length(Audios)), Audios).

search_audio(<<>>) ->  % {{{1
    get_audio();
search_audio("") ->  % {{{1
    get_audio();
search_audio(Term) ->  % {{{1
    Access = wf:user(),
    common:maybe_captcha(esocial_soundcloud:search_tracks(Access, wf:url_encode(unicode:characters_to_binary(Term)))).

order_playlist(RawAudio, PlayList) -> % {{{1
    AudioWithIds = [ {Id, Audio} || #esocial_track{id = Id} = Audio <- RawAudio],
    lists:foldr(fun(AID, A) ->
                      case proplists:get_value(AID, AudioWithIds) of
                          undefined -> A;
                          S -> [S|A]
                      end
                end,
                [],
                queue:to_list(PlayList)).

get_playlist_id(AID) -> %{{{1
    common:id_from_db("playlist", AID).

maybe_dashboard() -> % {{{1
    UID = common:uid(),
    Playlists = db:get_playlists(UID),
    if length(Playlists) >= 2 ->
           IsNotEmpty = (2 =< lists:foldr(fun(#playlist{songs=S}, A) ->
                                                 case queue:is_empty(S) of
                                                     true ->
                                                         A;
                                                     _ -> A + 1
                                                 end
                                          end,
                                          0,
                                          Playlists)),
           wf:info("IsNotEmpty: ~p", [IsNotEmpty]),
           if IsNotEmpty ->
                  common:tutorial(4, button_dashboard, "Start working", "Now you have everything to start working on your tasks. Click on <i class=\"fa fa-dashboard\"></i> and go on.", bottom);
              true -> ok
           end;
       true -> ok
    end.

normalize_song_id({_P, _Id}=SID) -> % {{{1
    SID;
normalize_song_id(SID) when is_integer(SID) -> % {{{1
    {vk, SID};
normalize_song_id(SID) -> % {{{1
    SID.

