%% -*- mode: nitrogen -*-
-module (index).
-compile(export_all).
-include_lib("nitrogen_core/include/wf.hrl").
-include("records.hrl").
-define(GETTEXT_DOMAIN, 'pomodoro-player').
-include_lib("gettexter/include/shortcuts.hrl").
-include_lib("esocial/include/esocial.hrl").
-include("db.hrl").
-define(REDIRECT_URI, wf:config(redirect_url)).

main() -> common:main().

title() -> "Welcome to Nitrogen".

body(undefined) ->  % {{{1
    State = wf:q(state),
    Code = wf:q(code),
    wf:info("Code: ~p", [Code]),
    #panel{
       class="row",
       body=login(State, Code)
      };
body(User) ->  % {{{1
    inner_body(User).

login_button(url) -> % {{{1
    RedirectURI = ?REDIRECT_URI,
    esocial:connect(soundcloud, RedirectURI, ["*"]);
login_button(slider) -> % {{{1
    RedirectURI = ?REDIRECT_URI,
    Class = ["btn", "btn-3"], 
    Text = ?_("Try It Now", wf:session_default(lang, <<"en_US">>)),
    #link{
       class=["img-btn" | Class],
       body=Text,
       url=esocial:connect(soundcloud, RedirectURI, ["*"])};
login_button(top) -> % {{{1
    RedirectURI = ?REDIRECT_URI,
    Class = ["btn-link", "btn-4"],
    #panel{
       style="display:inline-block;font-size:120%;border:1px #fff solid;border-radius:3px;margin:5px;",
       body=[
             #span{
                style="color: #fff; margin: 15px;",
                text=?_("Log in", wf:session_default(lang, <<"en_US">>))
               },
             #link{
                class=["img-btn" | Class],
                style="color: #fff; margin: 15px;",
                body="<i class='fa fa-soundcloud'></i>",
                url=esocial:connect(soundcloud, RedirectURI, ["*"])}
            ]}. 

login(State, Code) when State == undefined; Code == undefined ->   % {{{1
    login_button(slider);

login(State, Code) ->   % {{{1
    Platform = maybe_add_plan(State),

    {ok, #esocial{user_id=UID} = Access} = esocial:auth(wf:to_atom(Platform), wf:to_binary(Code), wf:to_binary(?REDIRECT_URI)),

    case db:maybe_add_user({wf:to_atom(Platform), UID}) of
        no_user ->
            login(Platform, Code);
        true -> 
            %wf:info("New user joined group ~p", [join_group(Access)]),
            wf:user(Access),
            wf:role(new, true),
            wf:redirect("/tasks?user=new");
        false ->
            wf:user(Access),
            wf:redirect("/")
    end.

inner_body(#esocial{platform=Platform, user_id=RUID}=Access) ->  % {{{1
    UID = {Platform, RUID},
    {PlayQueue, PlayList} = case db:get_playlists(UID) of
                                [#playlist{id=WorkId0}, #playlist{id=RestId0} |_] -> 
                                    WorkId = wf:session_default(work, WorkId0),
                                    RestId = wf:session_default(rest, RestId0),
                                    render_common_playlist(UID, WorkId, RestId);
                                _ -> {queue:new(), []}
                            end,
    %wf:info("PlayQueue: ~p; PlayList: ", [PlayQueue]),
    wf:session(playlist_queue, PlayQueue),
    common:tutorial(1, select_playlist, "Choose playlists", "You have several playlists now. It's time to choose which one will be playing during work sessions <i class=\"fa fa-edit\"></i> and breaks <i class=\"fa fa-coffee\"></i>.", left),
    #panel{
       class="row",
       body=[
             #panel{
                class=[
                       "col-xs-6",
                       %"col-xs-offset-1",
                       "col-sm-4",
                       "col-sm-offset-1",
                       "col-md-4",
                       "col-md-offset-1",
                       "col-lg-4",
                       "col-lg-offset-1"
                      ],
                body=#bootstrap_panel{
                        header=?_("CheckList", wf:session_default(lang, <<"en_US">>)),
                        table_id=checklist,
                        table=tasks:checklist(true)
                       }
               },
             #panel{
                class=[
                       "col-xs-6",
                       %"col-xs-offset-1",
                       "col-sm-5",
                       "col-sm-offset-1",
                       "col-md-5",
                       "col-md-offset-1",
                       "col-lg-5",
                       "col-lg-offset-1"
                      ],
                body=#bootstrap_panel{
                        header=?_("Player", wf:session_default(lang, <<"en_US">>)),
                        body=[
                              #panel{
                                 id = select_playlist,
                                 class=[
                                        "input-group",
                                        "select2-bootstrap-prepend"
                                       ],
                                 body=[
                                       #panel{
                                          class=[
                                                 "input-group-addon"
                                                ],
                                          body=#span{
                                                  class=[
                                                         "fa", 
                                                         "fa-edit"
                                                        ],
                                                  text=""
                                                 }},
                                       common:play_lists(UID, work)
                                      ]},
                              #panel{
                                 class=[
                                        "input-group",
                                        "select2-bootstrap-prepend"
                                       ],
                                 body=[
                                       #panel{
                                          class=[
                                                 "input-group-addon"
                                                ],
                                          body=#span{
                                                  class=[
                                                         "fa", 
                                                         "fa-coffee"
                                                        ],
                                                  text=""
                                                 }},
                                       common:play_lists(UID, rest)
                                      ]}
                             ],
                        table_id=playlist,
                        table = PlayList
                       }
               }
            ]}.


event({task_selected, TID}) ->  % {{{1
    wf:wire(#script{script="$('.wfid_checklist tr:first-child>td:first-child').popover('hide');"}),
    common:tutorial_next(),
    common:tutorial(3, timer_btn, "Congratulations!", "Congratulations!<br> You are ready to start working. Click on this button to start timer and dive into your task.<br> Good luck!"),
    wf:session(current_task, TID);

event({playlist_change, UID, _Id}) ->  % {{{1
    wf:wire(#script{script="$(obj('select_playlist')).popover('hide');"}),
    common:tutorial_next(),
    common:tutorial(2, ".wfid_checklist tr:first-child>td:first-child", "Choose task", "Choose task you will be working on by activating radio button."),
    WorkId = wf:q(work),
    RestId = wf:q(rest),
    {PlayQueue, PlayList} = render_common_playlist(UID, WorkId, RestId),
    wf:session(playlist_queue, PlayQueue),
    wf:update(playlist, PlayList);


event({play, URL}) ->  % {{{1
    common:reload(URL).

playlist_common(UID) -> % {{{1
    WorkId = wf:session(work),
    RestId = wf:session(rest),
    if WorkId == undefined ->
           {[], []};
       WorkId == RestId ->
           wf:wire(#alert{text="You selected one playlist for work and rest."}),
           {[], []};
       true ->
           Work = maybe_get_playlist_by_id(UID, work, WorkId),
           Rest = maybe_get_playlist_by_id(UID, rest, RestId),
           WorkList = playlist:get_audio(Work),
           RestList = playlist:get_audio(Rest),
           %wf:info("Work: ~p~n Rest: ~p", [WorkList, RestList]),
           merge_playlists(0, {WorkList, RestList}, {25 * 60, 5 * 60, 15 * 60}, {[0],[]})
    end.

merge_playlists(_, {W, R}, _, A) when W == []; R == [] -> % {{{1
    A;
merge_playlists(S, {[H | W], R}, {WT, RT, LRT}, {[L|LS], P}) when S rem 2 == 0, % {{{1
                                                                S =< 6,
                                                                WT > L -> 
    {_, #esocial_track{duration = D}=H1} = H,
    %wf:info("Merge: ~p, Total duration: ~p, Duration: ~p", [S, WT, D]),
    merge_playlists(S, {W, R}, {WT, RT, LRT}, {[L + D|LS], [H1|P]});
merge_playlists(S, {W, R}, {WT, RT, LRT}, {LS, P}) when S rem 2 == 0, % {{{1
                                                                S =< 6,
                                                                WT =< hd(LS) -> 
    %wf:info("Merge: ~p, Total duration: ~p", [S, WT]),
    merge_playlists(S + 1, {W, R}, {WT, RT, LRT}, {[0|LS], P});
merge_playlists(S, {W, [H | R]}, {WT, RT, LRT}, {[L|LS], P}) when S rem 2 == 1, % {{{1
                                                        S < 7,
                                                        RT > L -> 
    {_, #esocial_track{duration = D}=H1} = H,
    %wf:info("Merge: ~p, Total duration: ~p, Duration: ~p", [S, RT, D]),
    merge_playlists(S, {W, R}, {WT, RT, LRT}, {[L + D|LS], [H1|P]});
merge_playlists(S, {W, R}, {WT, RT, LRT}, {LS, P}) when S rem 2 == 1, % {{{1
                                                                S < 7,
                                                                RT =< hd(LS) -> 
    %wf:info("Merge: ~p, Total duration: ~p", [S, RT]),
    merge_playlists(S + 1, {W, R}, {WT, RT, LRT}, {[0|LS], P});
merge_playlists(7, {W, [H | R]}, {WT, RT, LRT}, {[L|LS], P}) when LRT > L -> % {{{1
    {_, #esocial_track{duration = D}=H1} = H,
    merge_playlists(7, {W, R}, {WT, RT, LRT}, {[L + D|LS], [H1|P]});
merge_playlists(7, {W, R}, {WT, RT, LRT}, {LS, P}) when LRT =< hd(LS) -> % {{{1
    %wf:info("Merge: ~p, Total duration: ~p", [7, LRT]),
    merge_playlists(0, {W, R}, {WT, RT, LRT}, {[0|LS], P});
merge_playlists(S, _L, T, A) -> % {{{1
    wf:error("Wrong call: (~p~n, p~n, ~p~n, ~p)", [S,  T, A]),
    A.


render_common_playlist(UID, WorkId, RestId) -> % {{{1
    wf:session(work, WorkId),
    wf:session(rest, RestId),
    wf:info("Work: ~p; Rest: ~p", [WorkId, RestId]),
    {TimeList, PlayLists} = playlist_common(UID),
    wf:info("PlayLists: ~p", [PlayLists]),
    wf:session(timer_periods, queue:from_list(lists:reverse(TimeList))),
    lists:foldl(fun(#esocial_track{
                       uri = URL,
                       id = AID
                      } = S, {Q, T}) ->
                        {queue:in_r(S, Q),
                         [playlist:list_row(S, false) | T]}
                end,
                {queue:new(), []},
                PlayLists).



maybe_get_playlist_by_id(UID, Id, PID) -> % {{{1
    case db:get_by_id(playlist, wf:to_integer(PID)) of
        [] ->
            [#playlist{id=NPID, songs=Songs} | _] = db:get_playlists(UID),
            wf:session(Id, NPID),
            Songs;
        #playlist{songs=Songs} ->
            Songs
    end.

maybe_add_plan(State) -> % {{{1
    case string:tokens(State, " ") of
        [P, S] ->
            wf:session(plan, S),
            P;
        [P|_] -> P
    end.
