%% -*- mode: nitrogen -*-
%% vim: ts=4 sw=4 et
-module (element_modal).
-include_lib("nitrogen_core/include/wf.hrl").
-include("records.hrl").
-export([
    reflect/0,
    render_element/1
]).

%% Move the following record definition to records.hrl:

-spec reflect() -> [atom()].
reflect() -> record_info(fields, modal).

-spec render_element(#modal{}) -> body().
render_element(_Record = #modal{
                            id=Id,
                            class=Class,
                            title=Title,
                            body=Body,
                            html_id=HtmlId,
                            buttons=Buttons
                           }) ->
    #panel{
       class=["modal", "fade"],
       html_id=HtmlId,
       %role="dialog",
       body=#panel{
               class="modal-dialog",
               body=#panel{
                       class="modal-content",
                       body=[
                             #panel{
                                class="modal-header",
                                body=[
                                      "<button type='button' class='close' data-dismiss='modal' aria-label='Close'><span aria-hidden='true'>&times;</span></button>",
                                      #h4{
                                         class="modal-title",
                                         body=Title
                                        }
                                     ]},
                             #panel{
                                class="modal-body",
                                body=Body
},
                             #panel{
                                class="modal-footer",
                                body=Buttons
}
                            ]}
              }
      }.
