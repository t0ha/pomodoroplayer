%% -*- mode: nitrogen -*-
%% vim: ts=4 sw=4 et
-module (action_bootstrap_popover).
-include_lib ("nitrogen_core/include/wf.hrl").
-include("records.hrl").
-export([
    render_action/1
]).


-spec render_action(#bootstrap_popover{}) -> actions(). % {{{1
render_action(_Record = #bootstrap_popover{
                            target = Id,
                            placement = Placement,
                            body = Body,
                            title = Title
                           }) ->
    Script = [
              "$(obj('", Id, "')).popover({",
              "'content': '", Body, "',",
              "'title': '", Title, "',",
              "'html': true,",
              "'container': 'body',",
              "'trigger': 'focus',"
              "'placement': '", wf:to_list(Placement), "'",
              "});"
             ],
    [
     #script{script=Script},

     #script{script="$(obj('" ++ Id ++ "')).popover('show')"}
    ].
