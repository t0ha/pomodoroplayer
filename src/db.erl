-module(db).
%%%
%%% Mnesia DB backend routines module.
%%% Put your backend code here
%%%

-compile([export_all]).
-include("db.hrl").

-define(V(Response), verify_create_table(Response)).

%% Don't remove! This is is used to install your Mnesia DB backend  from CLI tool
install([])->  % {{{1
    ?V(mnesia:create_table(profile,
                           [
                            {type, ordered_set},
                            {disc_copies, [node()]},
                            {attributes, record_info(fields, profile)}
                           ])),
    ?V(mnesia:create_table(playlist,
                           [
                            {type, ordered_set},
                            {disc_copies, [node()]},
                            {attributes, record_info(fields, playlist)},
                            {index, [profile_id, name]}
                           ])),
    ?V(mnesia:create_table(pomodoro,
                           [
                            {type, ordered_set},
                            {disc_copies, [node()]},
                            {attributes, record_info(fields, pomodoro)},
                            {index, [profile_id, task_id]}
                           ])),
    ?V(mnesia:create_table(account,
                           [
                            {type, ordered_set},
                            {disc_copies, [node()]},
                            {attributes, record_info(fields, account)},
                            {index, [profile_id]}
                           ])),
    ?V(mnesia:create_table(task,
                           [
                            {type, ordered_set},
                            {disc_copies, [node()]},
                            {attributes, record_info(fields, task)},
                            {index, [parent, profile_id, name]}
                           ])).

%% Don't remove! This is is used to update your Mnesia DB backend  from CLI tool
update([]) ->  % {{{1
    %% General update routine runs for current vsn
    {ok, VSN} = application:get_key(nitrogen, vsn),
    update(VSN);
update("0.1.0") ->  % {{{1
    %% Add config fields to all tables and user roles
    mnesia:transform_table(playlist,
                           fun(Record) when size(Record) == 5 ->
                                   LRecord = tuple_to_list(Record),
                                   list_to_tuple(LRecord ++ [#{}]);
                              (R) -> R
                           end,
                           record_info(fields, playlist)),
    mnesia:transform_table(task,
                           fun(Record) when size(Record) == 13 ->
                                   LRecord = tuple_to_list(Record),
                                   list_to_tuple(LRecord ++ [#{}]);
                              (R) -> R
                           end,
                           record_info(fields, task)),
    transaction(fun() ->
                       Profiles = mnesia:match_object(#profile{_='_'}),
                       lists:foreach(fun(Profile) ->
                                             mnesia:write(Profile#profile{roles=[]})
                                     end,
                                     Profiles)
               end);
update("0.1.1") ->  % {{{1
    %% Adds pomodoro table and data to it
    transaction(fun() ->
                        Pomodoros = mnesia:table_info(pomodoro, size),
                        if Pomodoros == 0 ->
                               Tasks = mnesia:select(task, [{#task{
                                                                done='$1',
                                                                failed='$2',
                                                                _='_'}, 
                                                             [{'or', 
                                                               {'>', '$1', 0},
                                                               {'>', '$2', 0}
                                                              }],
                                                             ['$_']}]),
                               lists:foreach(fun(#task{
                                                    id=TID,
                                                    profile_id=UID,
                                                    done = Done,
                                                    failed=Failed
                                                   }) ->
                                                     lists:foreach(fun(_) ->
                                                                           Id = next_id(pomodoro),
                                                                           mnesia:write(#pomodoro{
                                                                                           id=Id,
                                                                                           profile_id=UID,
                                                                                           task_id=TID,
                                                                                           created=calendar:universal_time(),
                                                                                           is_full = true,
                                                                                           comment="Exported from task"
                                                                                          })
                                                                   end,
                                                                   lists:seq(0, Done)),
                                                     lists:foreach(fun(_) ->
                                                                           Id = next_id(pomodoro),
                                                                           mnesia:write(#pomodoro{
                                                                                           id=Id,
                                                                                           profile_id=UID,
                                                                                           task_id=TID,
                                                                                           created=calendar:universal_time(),
                                                                                           is_full = false,
                                                                                           comment="Exported from task"
                                                                                          })
                                                                   end,
                                                                   lists:seq(0, Failed))
                                             end,
                                             Tasks);
                           true -> ok
                        end
                end);
update("0.1.2") ->  % {{{1
    mnesia:transform_table(playlist,
                           fun({playlist, Id, PID, Name, Songs, Map}) ->
                                   #playlist{
                                      id = Id,
                                      profile_id = PID,
                                      name = Name,
                                      songs = Songs,
                                      settings = Map};
                              (PL) -> PL
                           end, 
                           record_info(fields, playlist)),
    transaction(fun() ->
                        Users = mnesia:match_object(#profile{_='_'}),
                        lists:foreach(fun(#profile{id=PID}=Profile) when not is_tuple(PID) ->
                                              mnesia:delete_object(Profile),
                                              mnesia:write(Profile#profile{id={vk, PID}});
                                         (_) -> ok
                                      end,
                                      Users),
                        lists:foreach(fun(#profile{id=PID}) ->
                                              mnesia:write(#account{id=PID, profile_id=PID})
                                      end,
                                      Users),

                        Playlists = mnesia:match_object(#playlist{_='_'}),
                        lists:foreach(fun(#playlist{id=PID, profile_id=UID}=Playlist) when not is_tuple(PID) ->
                                              mnesia:delete_object(Playlist),
                                              mnesia:write(Playlist#playlist{profile_id={vk, UID}});
                                         (_) -> ok
                                      end,
                                      Playlists),

                        Tasks = mnesia:match_object(#task{_='_'}),
                        lists:foreach(fun(#task{id=PID, profile_id=UID}=Task) when not is_tuple(PID) ->
                                              mnesia:delete_object(Task),
                                              mnesia:write(Task#task{profile_id={vk, UID}});
                                         (_) -> ok
                                      end,
                                      Tasks),

                        Pomodoros = mnesia:match_object(#pomodoro{_='_'}),
                        lists:foreach(fun(#pomodoro{id=PID, profile_id=UID}=Pomodoro) when not is_tuple(PID) ->
                                              mnesia:delete_object(Pomodoro),
                                              mnesia:write(Pomodoro#pomodoro{profile_id={vk, UID}});
                                         (_) -> ok
                                      end,
                                      Pomodoros)

                end).
    
    

next_id(Type) ->  % {{{1
    transaction(fun() ->
                        case mnesia:last(Type) of
                            '$end_of_table' ->
                                1;
                            A -> 
                                A+1
                        end
                end).

get_accounts_for_user(UID) -> % {{{1
    transaction(fun() ->
                        mnesia:index_read(account, UID, #account.profile_id)
                end).

get_users() -> % {{{1
    transaction(fun() ->
                        mnesia:match_object(#profile{_='_'})
                end).

maybe_add_user(<<"">>) ->  % {{{1
    no_user;
maybe_add_user(UID) ->  % {{{1
    Now = calendar:universal_time(),
    transaction(fun() ->
                        case mnesia:read(account, UID) of
                            [] ->
                                Profile = #profile{
                                             id=UID,
                                             created=Now,
                                             last_login=Now
                                            },
                                mnesia:write(profile, Profile, write),
                                mnesia:write(account, #account{id=UID, profile_id=UID}, write),
                                true;
                            [#account{profile_id=PID}] -> 
                                [User] = mnesia:read(profile, PID),
                                Profile = User#profile{last_login=Now},
                                mnesia:write(profile, Profile, write),
                                lists:foreach(fun(Role) ->
                                                      wf:role(Role, true)
                                              end,
                                              User#profile.roles),
                                false
                        end
                end).

get_checklist(IDs) ->  % {{{1
    CheckListIds = sets:to_list(IDs),
    transaction(fun() ->
                        [ Task || Id <- CheckListIds,
                                  #task{state=S}=Task <- mnesia:read(task, Id),
                                  S /= done]
                end).

get_pomodoros_all(TID) ->  % {{{1
    transaction(fun() ->
                        Pomodoros =  mnesia:match_object(#pomodoro{task_id=TID, 
                                                                   _ = '_'}),
                        length(Pomodoros)
                end).

get_pomodoros_dayly(Tasks) ->  % {{{1
    {Date, _} = calendar:universal_time(),
    transaction(fun() ->
                        lists:map(fun(#task{id=TID}) ->
                                          Pomodoros =  mnesia:match_object(#pomodoro{task_id=TID, 
                                                                                     created={Date, '_'},
                                                                                     _ = '_'}),
                                          {TID, length(Pomodoros)}
                                  end,
                                  Tasks)
                end).
get_playlists(UID) -> % {{{1
    transaction(
      fun() ->
              mnesia:index_read(playlist, UID, #playlist.profile_id)
      end).

get_playlist(PID) -> % {{{1
    get_by_id(playlist, PID).

get_by_id(Table, ID) ->  % {{{1
    transaction(
      fun() ->
               hd(mnesia:read(Table, ID))
      end).

sync_playlist(#playlist{platform_id=PID, songs=Songs}=Playlist) -> % {{{1
    transaction(fun() ->
                        case mnesia:index_read(playlist, PID, #playlist.platform_id) of
                            [] ->
                                mnesia:write(Playlist#playlist{id=next_id(playlist)});
                            [PL|_] ->
                                mnesia:write(PL#playlist{songs=Songs})
                        end
                end).

new_playlist(Name, PID) -> % {{{1
    transaction(fun() ->
                        Id = next_id(playlist),
                        PlayList = #playlist{
                                      id=Id,
                                      name=Name,
                                      profile_id=PID,
                                      songs=queue:new()
                                     },
                        mnesia:write(playlist, PlayList, write),
                        PlayList
                end).

new_task(Name, Description, PID) -> % {{{1
    new_task(#task{name=Name,
                   description=Description,
                   profile_id=PID,
                   parent=0}).

new_task(Task) when is_record(Task, task) -> % {{{1
    transaction(fun() ->
                        Id = next_id(task),
                        TaskNew = Task#task{
                                  id=Id,
                                  created=calendar:universal_time()
                                 },
                        mnesia:write(task, TaskNew, write),
                        TaskNew
                end);
new_task(Task) -> % {{{1
    lager:warning("Wrong task: ~p", [Task]).

get_tasks(#task{profile_id=UID, id=TID}) -> % {{{1
    get_tasks(UID, TID);
get_tasks(UID) -> % {{{1
    get_tasks(UID, 0).

get_tasks(UID, #task{parent=Parent}) -> % {{{1
    get_tasks(UID, Parent);
get_tasks(UID, Parent) -> % {{{1
    transaction(
      fun() ->
              mnesia:select(task, [{#task{profile_id=UID,
                                          parent=Parent,
                                          state='$1',
                                          _='_'}, 
                                    [{'/=', '$1', 'done'}],
                                    ['$_']}])
      end).

mark_task_done(TID) when is_integer(TID) -> % {{{1
    transaction(
      fun() ->
              [Task] = mnesia:wread({task, TID}),
              mnesia:write(Task#task{state=done}),
              Task
      end).
add_pomodoro(TID, Full) -> % {{{1
    add_pomodoro(TID, Full, "No comments").

add_pomodoro(TID, Full, Comment) -> % {{{1
    transaction(fun() ->
                        [#task{profile_id=UID, done=P} = Task] = mnesia:wread({task, TID}),
                        Id = next_id(pomodoro),
                        mnesia:write(#pomodoro{
                                        id=Id,
                                        profile_id=UID,
                                        task_id=TID,
                                        created=calendar:universal_time(),
                                        is_full = Full,
                                        comment=Comment
                                       })
                end).


save(Item) ->  % {{{1
    transaction(fun() ->
                        mnesia:write(Item)
                end).
remove(Table, Id) -> % {{{1
    transaction(fun() ->
                        mnesia:delete({Table, Id})
                end).

%% For convenience in install and update process
verify_create_table({atomic, ok}) -> ok;  % {{{1
verify_create_table({aborted, {already_exists, _Table}}) -> ok.  % {{{1

% Wrapper around Mnesia transaction which returns empty list on errors
transaction(F) ->  % {{{1
    case mnesia:transaction(F) of
        {atomic, R} -> R;
        E -> 
            error_logger:warning_msg("Mnesia transaction error: ~p", [E]),
            []
    end.
                              

