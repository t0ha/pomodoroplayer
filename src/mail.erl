%% -*- mode: nitrogen -*-
%% vim: ts=4 sw=4 et
-module (mail).
-compile(export_all).
-include_lib("nitrogen_core/include/wf.hrl").
-include("records.hrl").

main() -> 
    case wf:q(subscribe) of
        undefined -> 
            Subject = wf:q(subject),
            From = wf:q(email),
            Name = wf:q(name),
            Text = wf:q(text),
            wf:info("Email from ~p<~p>, Subject: ~p, Text: ~p", [Name, From, Subject, Text]),
            smtp:send_html(From, "info@liquid-nitrogen.org", Subject, Text),
            wf:content_type("application/json"),
            "{\"success\": true}";
        Subscribe ->
            wf:info("Subscribe: ~p", [Subscribe]),
            From = "subscribe@pomodoro-player.com",
            smtp:send_html(From, "info@liquid-nitrogen.org", "Subscribtion to pomodoro-player", Subscribe),
            wf:content_type("application/json"),
            "{\"success\": true}"
    end.
