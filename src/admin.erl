%% -*- mode: nitrogen -*-
%% vim: ts=4 sw=4 et
-module (admin).
-compile(export_all).
-include_lib("nitrogen_core/include/wf.hrl").
-include_lib("esocial/include/esocial.hrl").
-include("records.hrl").
-include("db.hrl").

main() -> 
    common:main().

title() -> "Hello from admin.erl!".

body(Access) -> % {{{1
    case wf:role(admin) of
        false -> 
            wf:wire(#alert{text="You are not alowed to access admin interface. If you really should, please contact us by info@pomodoro-player.com" }),
            wf:redirect("/");
        true -> 
            Users = length(db:get_users()),
            #panel{
               class=["row"],
               body=[
                     #panel{
                        id=menu,
                        class=["col-md-2"],
                        body=[
                              #bootstrap_button{
                                 icon_type="fa",
                                 icon="users",
                                 delegate=?MODULE,
                                 postback=users
                                }, 
                              #span{body=[
                                          "users (", wf:to_list(Users), ")"
                                         ]}
                                          
                             ]},
                     #panel{
                        id=data,
                        class=["col-md-10"],
                        body=user_table(Access)
                       }
                    ]}
    end.
	
event({roles, UID, Id, VKUser}) -> % {{{1
    #esocial_profile{display_name=FullName} = VKUser,
    wf:info("UID: ~p", [UID]),
    Profile = db:get_by_id(profile, UID),
    wf:info("Profile: ~p", [Profile]),
    Roles = sets:from_list(Profile#profile.roles),
    wf:remove("#roles"),
    wf:insert_bottom("body", #modal{
                                html_id="roles",
                                title = <<"Roles edit for user ", FullName/bytes>>,
                                body=[
                                      #panel{
                                         body=#checkbox{id=user_roles,
                                                checked=sets:is_element(R, Roles),
                                                text=R,
                                                value=R
                                               }} 
                                      || R <- [pro, admin]
                                     ],
                                buttons=#bootstrap_button{
                                           icon_type="fa",
                                           icon="check",
                                           style="color: grey;",
                                           delegate=?MODULE,
                                           postback={save_roles, UID, Id, VKUser},
                                           popover="Ok"
                                          }
                               }),
    wf:wire(#script{script="$('#roles').modal('show')"});

event({save_roles, {Platform, _}=UID, Id, VKUser}) -> % {{{1
    Profile = db:get_by_id(profile, UID),
    NewRoles = wf:qs(user_roles),
    Roles = [wf:to_atom(R) || R <- NewRoles],
    db:save(Profile#profile{roles=Roles}),
    wf:replace(Id, render_user(VKUser, Platform)),
    wf:wire(#script{script="$('#roles').modal('hide')"});

event(click) ->
    wf:insert_top(placeholder, "<p>You clicked the button!").

user_table(#esocial{platform=Platform}=Access) -> % {{{1
    UIDs = [U || #profile{id={P,U}} <- db:get_users(), P == Platform],
    Profiles = esocial:profiles(Access,  UIDs),
    wf:info("Profiles: ~p", [Profiles]),
    #table{
       rows=#tablerow{
                cells=[
                       #tablecell{},

                       #tablecell{
                          text = wf:to_list("Name")
                         },
                       #tablecell{
                          text = wf:to_list("Platform")
                         },
                       #tablecell{
                          text = wf:to_list("Projects")
                         },
                       #tablecell{
                          text = "Playlists"
                         },
                       #tablecell{
                          text = "Registered"
                         },
                       #tablecell{
                          text = "Last seen"
                         },
                       #tablecell{
                          text = "Roles"
                         }
                      ] ++ [render_user(U, Platform) || U <- Profiles]
               }
               }.

render_user(#esocial_profile{  % {{{1
               id = UID,
               display_name = FullName,
               profile_uri=URL,
               photo = Photo
             }=VKUser, Platform) ->
    #profile{
       id=PID,
       roles=Roles,
       last_login={Last, _},
       created={Created, _}
      } = db:get_by_id(profile, {Platform, UID}),
    Projects = db:get_tasks(PID),
    NProjects = length(Projects),
    NTasks = lists:foldl(fun(T, A) ->
                                 A + length(db:get_tasks(T))
                         end,
                         0,
                         Projects),
    PlayLists = length(db:get_playlists(PID)),
    Id=wf:to_atom(wf:f("user~p", [UID])),
    #tablerow{
       id=Id,
       cells=[
              #tablecell{
                 body = #image{image=Photo}
                },
              #tablecell{
                 body=#link{
                         url=URL,
                         text = FullName
                        }
                },
              #tablecell{
                 text = wf:to_list(Platform)
                },
              #tablecell{
                 body = [
                         wf:to_list(NProjects),
                         " (",
                         wf:to_list(NTasks),
                         ")"
                        ]
                },
              #tablecell{
                 text = wf:to_list(PlayLists)
                },
              #tablecell{
                 text = common:render_date(Created)
                },
              #tablecell{
                 text = common:render_date(Last)
                },
              #tablecell{
                 body=#link{
                         postback={roles, PID, Id, VKUser},
                         delegate=?MODULE,
                         text = "[" ++ string:join([wf:to_list(R) || R<- Roles], ", ") ++ "]"
                        }
                }
             ]}.
